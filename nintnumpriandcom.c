#include <stdio.h>
int num()
{
    int n1;
    printf("Enter N\n");
    scanf("%d",&n1);
    return n1;
}

void input(int n1,int a[])
{
    int i;
    printf("Enter the numbers\n");
    for(i=0;i<n1;i++)
    {
        scanf("%d",&a[i]);
    }
}

int compute(int n1,int a[])
{
    int i,c,p=0,j;
    for(i=0;i<n1;i++)
    {
        c=0;
        for(j=1;j<a[i];j++)
        {
            if(a[i]%j==0)
                c++;
        }
        if(c==2)
            p++;
    }
    return p;
}

void output(int p,int c)
{
    printf("Number of primes : %d\nNumber of composites : %d\n",p,c);
}

int main()
{
    int n1,p;
    n1=num();
    int a[n1];
    input(n1,a);
    p=compute(n1,a);
    output(p,n1-p);
    return 0;
}