#include <stdio.h>

int input()
{
    int a;
    printf("Enter the number\n");
    scanf("%d",&a);
    return a;
}

int compute(int a)
{
    int i,c=0;
    for(i=1;i<=a;i++)
    {
        if(a%i==0)
            c++;
    }
    return c;
}

void output(int a,int c)
{
    if(c==2)
        printf("%d is a prime number\n",a);
    else
        printf("%d is a composite number\n",a);
}

int main()
{
    int a,c;
    a=input();
    c=compute(a);
    output(a,c);
    return 0;
}