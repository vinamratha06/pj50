#include<stdio.h>
void get_array(int n,int a[n])
{
 	printf("Enter the elements into the array:\n");
   for(int i=0;i<n;i++)
        {
            scanf("%d",&a[i]);
        }
}
void output1(int n, int a[n])
{  
    printf("The elements of the array are:\n");
    for(int i=0;i<n;i++)
        {
            printf("%d ",a[i]);
        }
    printf("\n");
}
void compute(int n, int a[n], int *small, int *spos, int *large, int *lpos)
{
    *small=a[0];
    for(int i=1;i<n;i++)
        {
            if(a[i]<*small)
                {
                    *small=a[i];
                    *spos=i;
                }
        }
    *large=a[0];
    for(int i=1;i<n;i++)
        {
            if(a[i]>*large)
                {
                    *large=a[i];
                    *lpos=i;
                }
        }
}
void output2(int small, int large, int spos, int lpos)
{
        printf("\nBefore interchanging:\n");
        printf("The smallest number in the array is %d at the position %d \n",small,spos+1);
        printf("The largest number in the array is %d at the position %d \n",large,lpos+1);
}
void interchange(int n, int a[n], int spos, int lpos)
{   
	 int temp;     
    temp=a[spos];
    a[spos]=a[lpos];
    a[lpos]=temp;
}
void output3(int n, int a[n])
{
    printf("\nAfter interchanging:\n");
    printf("The elements of the array are:\n");
    for(int i=0;i<n;i++)
        {
            printf("%d ",a[i]);
        }
}
int main()
{
	int n,small,spos=0,large,lpos=0;
	printf("Enter the size of the array:\n");
    scanf("%d",&n);
    int a[n];
	get_array(n,a);
	output1(n,a);
	compute(n,a,&small,&spos,&large,&lpos);
	output2(small,large,spos,lpos);
	interchange(n,a,&spos,&lpos);
	output3(n,a);
   return 0;
}