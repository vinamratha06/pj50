#include <stdio.h>

int main()
{
	int mid,right,left,n,f=0;
    printf("enter the value of n");
    scanf("%d",&n);
    int elements[n],i,e;
    for(i=0;i<n;i++)
    {
        printf("enter the element");
        scanf("%d",&elements[i]);
    }
    printf("enter the search element");
    scanf("%d",&e);
    left=0;
    right=n-1;
    while(left<=right)
    {
        mid=(right+left)/2;
        
        if(e==elements[mid])
        {
            f=1;
            printf("Element is found in the position %d",mid+1);
            break;
        }
        if(elements[mid]>e)
        {
            right = mid -1;
        }
        else 
        {
            left = mid + 1;
        }         
    }
    if(f==0)
        printf("\nElement not found");   
    return 0;
}
