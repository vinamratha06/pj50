#include<stdio.h>
#include<math.h>
float inputx()
{
   float x;
   printf("Enter the x coordinate of the point:\n ");
   scanf("%f",&x);
   return x;
}
float inputy()
{
   float y;
   printf("Enter the y coordinate of the point:\n");
   scanf("%f",&y);
   return y;
}
float distance(float x1,float y1,float x2,float y2)
{
   float dis;
   dis=sqrt(pow(x2-x1,2)+pow(y2-y1,2));
   return dis;
}
void display(float result,float x1,float y1,float x2,float y2) 
{
   printf("Distance between (%f,%f) and (%f,%f) is %f",x1,y1,x2,y2,result);
}
int main()
{
   float x1, x2, y1, y2;
   float dis_btw_2_pts;
   x1=inputx();
   y1=inputy();
   x2=inputx();
   y2=inputy();
   dis_btw_2_pts=distance(x1,y1,x2,y2);
   display(dis_btw_2_pts,x1,y1,x2,y2);
   return 0;
}
    